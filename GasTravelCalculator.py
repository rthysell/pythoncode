"""

Title: Gas Travel Calculator
Author: Rachelle Thysell
Date: January 7, 2016

This program calculates the amount of money a person would spend on gas depending upon their expected travel
distance (in miles), the current price of gas, their car's miles per gallon, and how many passengers are expected
to travel with them. If there is more than one passenger, the program also prints out what the expected cost would
be for each person if they were to split the bill.

This program happens to be a copy of a program I wrote in my first semester of Computer Science at SSU. At the
time, I wrote the program in C++, so I've decided to now write it for Python.

"""


def main():

    expected_miles = float( input( "How many miles will your trip take one way? " ) )
    round_trip = input( "Are these the same amount of miles round trip? " )
    
    if round_trip == "No" or round_trip == "no":
        additional_miles = float( input( "How many miles is the return trip? " ) )
        expected_miles += additional_miles
    else:
        expected_miles *= 2

    car_mpg = float( input( "How many miles does your car get per gallon of gas? " ) )
    
    current_gas_price = float( input( "What is the average cost of gas right now? " ) )

    num_passengers = int( input( "How many passengers (including yourself) are traveling with you? " ) )

    print()

    print( "Please note that these numbers only represent estimates of what it may cost you for gas on your trip." )
    print( "Gas prices are likely to vary depending on which gas station you go to and what savings they may have." )
    print( "However, these numbers will give you an idea for how much you'll be likely to spend." )

    print()

    print( "Here is the information you provided." )
    print( "Expected miles (with additional, if return trip is different): %f" %  expected_miles )
    print( "MPG of your car: %f" %  car_mpg )
    print( "Current (average) price of gas: %f" % current_gas_price )
    print( "Number of passengers: %d" % num_passengers )

    print()

    print( "If the information is incorrect, please restart the program." )

    print()

    cost = ( ( expected_miles / car_mpg ) * current_gas_price ) / num_passengers

    print( "The total cost of gas would be $" + str( cost ) + " per person." )




main()



