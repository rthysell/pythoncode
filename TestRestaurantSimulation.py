"""

Title: Test Restaurant Simulation
Author: Rachelle Thysell
Date: January 7, 2016

This program tests the math done in the Restaurant Simulation program.

"""

#from RestaurantSimulation import *

def main():

    filename = "TRS.txt"
    text = open( filename, 'r' )
    
    file_input = text.readlines()

    parties = [{},{},{},{}]
    i = 1
    x = {}
    for line in file_input:
        l = line.split()
        if l:
            key = l[0]
            val = float( l[1] )
            if key == "num_guests":
                x = parties[ i ] 
            x[ key ] = val
        else:
            parties[i] = x
            x = {}
            i += 1
    print( parties[ 0 ] )
main()
