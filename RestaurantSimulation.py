"""

Title: Restaurant Simulation
Author: Rachelle Thysell
Date: January 7, 2016

This program simulates the process of a restaurant. Specifically, it calculates the total amount of time a dining party
arrives to when they leave. It factors in the speed of the server, the wait time for seating, food, dessert, and ordering,
and the amount of time the party stays at the table at the end of their meal to when they leave the restaurant.

"""


def Simulate():
    single_party_total_time = 0
    num_parties = 10
    total_party_total_time = 0
    waiter_efficiency = 2
    order_time = 0.75

    for i in range( 1, num_parties + 1 ):

        single_party_num_guests = int( input( "How many people are in your party? " ) )
        seating_wait = float( input( "How long did the party have to wait to be seated? " ) )
        seating_time = float( input( "How long did it take to seat the party? " ) )
    
        single_party_total_time += seating_wait + seating_time + waiter_efficiency

        prev_food_wait = 0

        for i in range( 1, single_party_num_guests + 1 ):
            food_wait = float( input( "How long did guest " + str(i) + " have to wait for their meal? " ) )
            if food_wait > prev_food_wait:
                prev_food_wait = food_wait
            single_party_total_time += order_time
        single_party_total_time += prev_food_wait + waiter_efficiency

        eating_time = float( input( "How long did it take the party to finish their meal? " ) )
    
        guests_who_want_dessert = int( input( "How many people in the party would like dessert? " ) )
    
        single_party_total_time += eating_time

        prev_food_wait = 0
        for i in range( 1, guests_who_want_dessert + 1 ):
            dessert_wait = float( input( "How long did guest " + str(i) + " have to wait for their dessert? " ) )
            if dessert_wait > prev_food_wait:
                prev_food_wait = dessert_wait
            single_party_total_time += order_time
        single_party_total_time += prev_food_wait

        dessert_serve_time = float( input( "How long did it take to serve the guests their dessert? " ) )
        end_of_meal_time = float( input( "How long did the party stay at their table before leaving? " ) )

        single_party_total_time += dessert_serve_time + end_of_meal_time

        print( "For a party of %d guests, the total amount of time they spent in the restaurant from the time they arrived to the time they left was %.2f minutes." % (single_party_num_guests, single_party_total_time ) )

    print( "The average dining time for %d parties in simulation: %.2f minutes." % ( num_parties, total_party_total_time / num_parties ) )
