
Name: Rachelle Thysell
Date: January 7, 2016

The files within this repository are separate and not meant to be combined into a single project. They are simply
snippets of code that I wrote in my spare time to do various things. Each file will be listed below with a short
description of what the program within does.


GasTravelCalculator.py
    This program calculates the total amount of money you would spend on gas when traveling. It factors in your travel
    miles, your car's mpg, the current price of gas, and how many passengers you have with you.

CashRegister.py
    This program simulates a simple cash register. The user inputs the cost of the meal and how much of it has been paid.
    The program calculates the sales tax and total cost for the meal and lets the user know how much money they owe or
    are owed. This program repeats until there are no more customers to process that day, then displays how many
    customers have been processed and the total cost of all their meals.
