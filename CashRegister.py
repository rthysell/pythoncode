"""

Title: Cash Register
Author: Rachelle Thysell
Date: January 7, 2016

This program simulates a simple cash register. It calculates the sales tax and total price of a meal, then displays the
difference between what the customer paid and how much the meal cost. The program should repeat every time the user
says there is another customer to process. At the end of the program, it should display the total number of customers
and the cost of all the meals processed.

"""


def main():

    another_customer = input( "Do you have a customer to process? (Please answer Yes or No) " )
    number_of_customers = 0
    total_cost_of_meals_processed = 0

    while another_customer.lower() == "yes":
        number_of_customers += 1
        meal_price = float( input( "Please enter the total cost of the meal: $" ) )
        sales_tax = meal_price - ( meal_price * 0.825 )
        total_price = meal_price + sales_tax
        print( "The total cost of this meal is $%.2f" % total_price )
        total_cost_of_meals_processed += total_price
        paid_amount = float( input( "Please enter the amount given by the customer: $" ) )
        while paid_amount < total_price:
            print( "The customer still owes $%.2f" % ( total_price - paid_amount ) )
            paid_amount += float( input( "How much more has the customer given? $" ) )
        amount_owed = paid_amount - total_price
        print( "The customer's change is $%.2f" % amount_owed )

        another_customer = input( "Do you have another customer to process? (Please answer Yes or No) " )

    print( "The total number of customers processed for today is: %d" % number_of_customers )
    print( "The total cost of meals bought for today is: $%.2f" % total_cost_of_meals_processed )



main()





